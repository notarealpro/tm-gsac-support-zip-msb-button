// ==UserScript==
// @name         Download Support Zip from GSAC
// @namespace    https://getsupport.atlassian.com
// @version      0.5
// @description  Download all ticket attachments using the MSB download all endpoint. It will open the finder windows as per normal and copy the ticket # to the clipboard for quick pasting to parse zip.
// @author       Danny Mark
// @match        https://getsupport.atlassian.com/browse/*
// @downloadURL  https://bitbucket.org/notarealpro/tm-gsac-support-zip-msb-button/raw/master/Download_Support_Zip_from_GSAC.user.js
// @updateURL    https://bitbucket.org/notarealpro/tm-gsac-support-zip-msb-button/raw/master/Download_Support_Zip_from_GSAC.user.js
// @grant        GM_setClipboard
// @grant        GM_addStyle
// ==/UserScript==

(function () {
    'use strict';

    var context = window.location.pathname;
    var ticket = context.substring(8);
    // Create buttons(#ID, [text] )
    createBtn("downZip", "Support Zip");
    createBtn("openFolder", "Open Folder");
    
    remoteClickMSB();
    detectNewDOM();
    

    function remoteClickMSB() {
        // Download attachments fn
        $('#downZip').click(function () {
            GM_setClipboard(ticket); // Copies issues key to clipboard
            fetch('http://127.0.0.1:3017/ticket/' + ticket).then(console.log);
            alert('Files downloading - Please wait 5-10 sec. \n------------------------------------------\nIt will happen if MSB is working.\nOr at least should. ');
        });

        $('#openFolder').click(function () {
            fetch('http://127.0.0.1:3017/ticketo/' + ticket).then(console.log);
        });
    }

    function createBtn(id, text) {
        var btn = document.createElement("BUTTON");
        btn.id = (id);
        btn.type = "button";
        btn.className = "gsacMsbBtn";
        //btn.style = "border: 1px solid #ccc;border-radius: 3px;padding: 4px 10px;margin: 0 5px 0 0;background:#f5f5f;background-color:#f5f5f5;color:#333;height:30px;font-size: 14px;button:hover {background-color: yellow;}";
        var t = document.createTextNode(text);  // Create a text node
        btn.appendChild(t);                     // Append the text to <button>
        document.getElementById("opsbar-opsbar-transitions").appendChild(btn); // GSAC Toolbar
    }

    function detectNewDOM() {
        
        var target = document.querySelector('#stalker');
        // create an observer instance
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutation) {
                console.log(mutation.type + "Mutation detected.");
                createBtn("downZip", "Support Zip");
                createBtn("openFolder", "Open Folder");
                remoteClickMSB();
            });
        });
        // configuration of the observer:
        var config = {
            childList: true
        };
        // pass in the target node, as well as the observer options
        observer.observe(target, config);
    }

    GM_addStyle(`
    .gsacMsbBtn {
        border:                 0px;
        padding:                4px 10px;
        margin:                 0 0 0 2px;
        background-color:       rgba(9,30,66,0.08);
        color:                  #344563;
        height:                 30px;
        font-size:              14px;
    }
    .gsacMsbBtn:hover {
        background-color:       rgba(9,30,66,0.13) !important;
        border-color:           transparent;
    }
    .gsacMsbBtn:first-child {
        border-top-left-radius: 3.01px;
        border-bottom-left-radius: 3.01px;

    }
    .gsacMsbBtn:last-child {
        border-top-right-radius: 3.01px;
        border-bottom-right-radius: 3.01px;
    }
    #downZip.gsacMsbBtn {
    margin: 0 0 0 10px;
        border-top-left-radius: 3.01px;
        border-bottom-left-radius: 3.01px;
    }
    #opsbar-transitions_more {
        border-top-right-radius: 3.01px;
        border-bottom-right-radius: 3.01px;
    }
`);

})();